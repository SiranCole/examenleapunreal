# Pregunta 6

## ¿Cómo implementaría una versión de este power up si no tuviera a la mano la lista de enemigos activos en escena?

### Trataría cada rayo de interlink como un objeto que el Enemy posiciona en sus pies, el cual tenga un radio de colisión X y que al colisionar con un enemigo, este reciba el daño y tenga la posibilidad de crear un nuevo rayo. De esta forma, ya no tendríamos que buscar en una lista de enemigos, el rayo mismo encontraría su objetivo al colisionar con un enemigo.