# Instrucciones

## Pregunta 1: 

### La respuesta es texto en formato markdown

## Pregunta 2:

### Tener instalado g++, ubicar la línea de comandos en la carpeta "Pregunta 2" y utilizar el siguiente comando para compilar y ejecutar el programa:

    g++ main.cpp -o out.exe && ./out.exe

### El programa pedirá ingresar los datos de los rectángulos y finalmente mostrará en consola las colisiones existentes.

## Pregunta 3:

* Utilicé OpenGL para graficar los puntos requeridos. Al iniciar el programa, se solicitan ingresar n, r, Px(coordenada x del centro) y Py(coordenada y del centro).
* Los datos a excepción de n se ingresan en pixeles. Como referencia, la ventana tiene 640 x 480 pixeles.

## Pregunta 5:

### El algoritmo se encuentra en la función lockOnAlgorithm. Dado que en el enunciado se explica que se tomará en cuenta el código mas no la evidencia del funcionamiento, no realicé una interacción en consola. De todas formas, el código puede ser ejecutado de la misma forma que el de la Pregunta 2. Este mostrará en consola el arreglo de prioridades utilizado y la posición del enemigo idóneo.

## Pregunta 7: 

### La respuesta es texto en formato markdown

## Pregunta 8:

### El proyecto Orquesta deberá ser abierto con Visual Studio 2019. Utilicé la librería SDL para procesar el input de forma paralela.