# Pregunta 7

### El juego instancia frecuentemente enemigos que el jugador derrotaba rápidamente. Al ser derrotados, los enemigos eran destruidos de la escena. Sin embargo, la creación y destrucción de objetos en tiempo de ejecución impacta seriamente el rendimiento del juego. Explique según su criterio, la mejor solución para este problema.

<br>

## Respuesta:

### La mejor solución sería reutilizar los enemigos. En lugar de destruir a los enemigos derrotados, deberían guardar estos enemigos en una cola de reciclaje, para luego, en lugar de crear un nuevo enemigo, obtener el primero de dicha cola y reinicializarlo con los parámetros del nuevo. Solo se crearán nuevas instancias cuando no hayan enemigos que reciclar(antes de que ningún enemigo haya sido derrotado).