#pragma once
#include <vector>
#include <string>
struct Attack
{
    
    float attackDuration;
    std::vector<std::string> attackTags;
    std::vector<std::string> reactionTags;

    Attack(float ad, std::vector<std::string> at, std::vector<std::string> rt) : attackDuration(ad), attackTags(at), reactionTags(rt) {}
};

