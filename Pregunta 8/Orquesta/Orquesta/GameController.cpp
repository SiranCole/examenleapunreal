#include "GameController.h"

void GameController::run()
{
    update();
}
void GameController::update()
{
    while (true)
    {
        processInput();
        for (auto e : *activeEnemies) {
            e->update();
        }
        os->update();
    }
}

void GameController::processInput()
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
                exit(0);
                break;
            case SDLK_q:
                activeEnemies->at(0)->attack(os);
                break;
            case SDLK_w:
                activeEnemies->at(1)->attack(os);
                break;
            case SDLK_t:
                activeEnemies->at(2)->attack(os);
                break;
            }
        }
    }
}


GameController::GameController()
{
    
    activeEnemies = new std::vector<Enemy*>();
    os = new OrquestaSystem(4, 60, 5, activeEnemies);
    activeEnemies->push_back(new Enemy("Rana", "Terrestre",
        std::vector<Attack*>({ new Attack(5,
                                          std::vector<std::string>({"flincher", "knockdown"}),
                                          std::vector<std::string>({"bullet"})) }), os));

    activeEnemies->push_back(new Enemy("Tronco", "Terrestre",
        std::vector<Attack*>({ new Attack(8,
                                          std::vector<std::string>({"charger", "distance"}),
                                          std::vector<std::string>({"knockdown"})) }), os));

    activeEnemies->push_back(new Enemy("Mono", "Aereo",
        std::vector<Attack*>({ new Attack(2,
                                          std::vector<std::string>({"distance", "bullet"}),
                                          std::vector<std::string>({"charger"})) }), os));

    SDL_Window* window = SDL_CreateWindow("Ataque Orquesta", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 100, 100, 0);
    
}

GameController::~GameController()
{
}