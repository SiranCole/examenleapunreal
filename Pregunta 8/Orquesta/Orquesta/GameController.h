#pragma once
#include <SDL\SDL.h>
#include <ctime>
#include "Enemy.h"

class GameController
{
private:
    std::vector<Enemy*>* activeEnemies;
    OrquestaSystem* os;
    SDL_Window* window;
public:
    GameController();
    ~GameController();
    void run();
    void update();
    void processInput();
};

