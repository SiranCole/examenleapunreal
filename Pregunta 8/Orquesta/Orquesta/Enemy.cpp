#include "Enemy.h"

Enemy::Enemy(std::string name, std::string et, std::vector<Attack*> ea, OrquestaSystem* os)
    : name(name), enemyType(et), EnemyAttack(ea), isBlocked(false), isAttacking(false), os(os)
{
    nextAttackTime = time(NULL);
}

void Enemy::attack(OrquestaSystem* os)
{
    if (!isBlocked) {
        os->attackSolicitude(this);
    }
    
}

void Enemy::update()
{
    if (isAttacking) {

        if (time(NULL) > nextAttackTime) {
            isAttacking = false;
            os->attackEnd(this);
        }
    }
}
