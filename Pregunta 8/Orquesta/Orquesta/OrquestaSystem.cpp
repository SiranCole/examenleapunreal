#include "OrquestaSystem.h"
#include "Enemy.h"
#include <algorithm>

OrquestaSystem::OrquestaSystem(int N, int R, int T, std::vector<Enemy*>* ae): N(N), R(R), T(T), activeEnemies(ae), cooldown(false)
{
    currentN = 0;
    currentT = time(NULL);
}

OrquestaSystem::~OrquestaSystem()
{
}

void OrquestaSystem::attackSolicitude(Enemy* enemy)
{
    std::cout << "El enemigo " << enemy->name << " ha intentado atacar." << std::endl;

    if (enemiesAttacking[enemy->enemyType] != 0) {
        std::cout << "El sistema no ha permitido el ataque del enemigo " << enemy->name << " porque un enemigo de su mismo tipo est� atacando."<< std::endl;
        return;
    }

    std::cout << "El sistema ha permitido el ataque del enemigo " << enemy->name << std::endl;

    enemy->nextAttackTime = time(NULL) + (time_t)(enemy->EnemyAttack[0]->attackDuration);
    enemy->isAttacking = true;
    enemy->isBlocked = true;
    enemiesAttacking[enemy->enemyType] = 1;
    

    for (int i = 0; i < activeEnemies->size(); i++) {
        Enemy* e = activeEnemies->at(i);
        if (enemy != e && enemy->enemyType == e->enemyType) {
            e->isBlocked = true;
        }
    }

    if (currentN != 0) {
        currentN--;
        if (currentN == 0)
            std::cout << "El n�mero m�nimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado.\n";
    }

    if (cooldown || currentN != 0) {
        return;
    }
    srand(time(NULL));
    int aleatorio = rand() % 100;
    if (aleatorio >= R) {
        return;
    }

    for (int i = 0; i < activeEnemies->size(); i++) {
        Enemy* e = activeEnemies->at(i);
        if (enemy != e && enemy->enemyType == e->enemyType) {
            
            std::vector<std::string>::iterator it;
            for (auto atag : enemy->EnemyAttack[0]->attackTags) {
                std::vector<std::string> rtags = e->EnemyAttack[0]->reactionTags;
                it = std::find(rtags.begin(), rtags.end(), atag);
                if (it != rtags.end()) {
                    enemiesAttacking[enemy->enemyType] = 2;
                    e->isAttacking = true;
                    e->nextAttackTime = time(NULL) + (time_t)e->EnemyAttack[0]->attackDuration;
                    currentT = time(NULL) + (time_t)T;
                    currentN = N;
                    cooldown = true;
                    std::cout << "El sistema ha originado un ataque de orquesta entre el enemigo " << enemy->name << " y " << e->name << "." << std::endl;
                    return;
                }
            }
        }
        
    }

}

void OrquestaSystem::attackEnd(Enemy* enemy)
{
    enemiesAttacking[enemy->enemyType] -= 1;   
    if (enemiesAttacking[enemy->enemyType] == 0) {
        std::cout << "La ventana de bloqueo de ataque generada por el ataque del enemigo " << enemy->name << " ha terminado." << std::endl;
        for (int i = 0; i < activeEnemies->size(); i++) {
            Enemy* e = activeEnemies->at(i);
            if (enemy->enemyType == e->enemyType) {
                e->isBlocked = false;
            }
        }
    }
}

void OrquestaSystem::update()
{
    if (cooldown && time(NULL) > currentT) {
        std::cout << "La ventana de tiempo de bloqueo de ataque en orquesta ha terminado.\n";
        cooldown = false;
    }
}
