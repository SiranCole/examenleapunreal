#pragma once
#include "Attack.h"
#include "OrquestaSystem.h"
#include <iostream>
#include <ctime>

class Enemy
{
private:
    friend class OrquestaSystem;
    std::string name;
    std::string enemyType;
    std::vector<Attack*> EnemyAttack;
    bool isBlocked;
    bool isAttacking;
    time_t nextAttackTime;
    OrquestaSystem* os;
public:
    Enemy(std::string name, std::string et, std::vector<Attack*> ea, OrquestaSystem* os);

    void attack(OrquestaSystem* os);
    void update();
    
};
