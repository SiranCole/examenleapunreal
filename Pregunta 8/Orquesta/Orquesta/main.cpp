#include "GameController.h"

int main(int argc, char** argv)
{
    GameController gc;
    gc.run();
    return 0;
}