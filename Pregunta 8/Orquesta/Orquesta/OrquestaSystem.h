#pragma once
#include "Enemy.h"
#include <map>

class Enemy;

class OrquestaSystem
{
private:
	int N;
	int R;
	int T;
	int currentN;
	time_t currentT;
	bool cooldown;
	std::map<std::string, int> enemiesAttacking;
	std::vector<Enemy*>* activeEnemies;
public:
	OrquestaSystem(int N, int R, int T, std::vector<Enemy*>* ae);
	~OrquestaSystem();

	void attackSolicitude(Enemy* enemy);
	void attackEnd(Enemy* enemy);
	void update();
};

