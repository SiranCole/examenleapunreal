#include <bits/stdc++.h>

#define PI 3.14159265
#define distEfect 20
#define visionAngle PI/20

struct Vec3{
    int x, y, z;
    Vec3(int x, int y, int z) : x(x), y(y), z(z){}
};
struct Player{
    Vec3 pos;
    Vec3 pForward;
    Player(Vec3 pos, Vec3 pF) : pos(pos), pForward(pF){}
};
struct Enemy{
    Vec3 pos;
    bool isRendered;
    Enemy(int x, int y, int z, bool ir) : pos(x,y,z) {
        this->isRendered = ir;
    }
};

float dot(Vec3 a, Vec3 b){
    return a.x * b.x + a.y * b.y + a.z * b.z;
}
float magnitude(Vec3 a)  //calculates magnitude of a
{
    return std::sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

bool isEfective(Vec3 pos, Vec3 pForward, Enemy* enemy){
    Vec3 enemyPosFromPlayer = Vec3(enemy->pos.x - pos.x,
                                    enemy->pos.y - pos.y,
                                    enemy->pos.z - pos.z);
    float enemyDistFromPlayer = std::sqrt(enemyPosFromPlayer.x * enemyPosFromPlayer.x +
                                enemyPosFromPlayer.y * enemyPosFromPlayer.y +
                                enemyPosFromPlayer.z * enemyPosFromPlayer.z);
    return (distEfect >= enemyDistFromPlayer);
}

bool inVisionCone(Vec3 pos, Vec3 pForward, Enemy* enemy){
    Vec3 enemyPosFromPlayer = Vec3(enemy->pos.x - pos.x,
                                    enemy->pos.y - pos.y,
                                    enemy->pos.z - pos.z);

    float angle = std::acos(dot(enemyPosFromPlayer, pForward) / (magnitude(enemyPosFromPlayer) * magnitude(pForward)));
    return visionAngle >= angle;
}

// LOCK-ON SYSTEM
Enemy* lockOnAlgorithm(Vec3 pos, Vec3 pForward, std::vector<Enemy*> activeEnemies){

    Enemy* idoneo = nullptr;
    // Si no hay enemigos retorna nullptr
    if(activeEnemies.size() == 0)
        return idoneo;
    
    int priority[activeEnemies.size()] = {0};

    for(int i = 0; i < activeEnemies.size(); i++){
        if(activeEnemies[i]->isRendered && isEfective(pos, pForward, activeEnemies[i])){
            priority[i] = 1;    
            if(inVisionCone(pos, pForward, activeEnemies[i])){
                priority[i] = 2;
            }
        }
    }

    for(auto i: priority){
        std::cout << i << " ";
    }
    std::cout<<std::endl;
    

    int prioIndex = 0;
    for(int i = 1; i < activeEnemies.size(); i++){
        if(priority[i] > priority[prioIndex]){
            prioIndex = i;
        }    
    }

    if(priority[prioIndex] == 0){
        return nullptr;
    }
    return activeEnemies[prioIndex];
}


int main(){
    Player* player = new Player(Vec3(0,0,0), Vec3(1,0,0));
    std::vector<Enemy*> activeEnemies = std::vector<Enemy*>();
    activeEnemies.push_back(new Enemy(21,1,0, true));
    activeEnemies.push_back(new Enemy(5,5,5, true));
    activeEnemies.push_back(new Enemy(1,0,0, false));
    activeEnemies.push_back(new Enemy(18,2,0, true));

    Enemy* result = lockOnAlgorithm(player->pos, player->pForward, activeEnemies);
    if(result!=nullptr){
        std::cout << "Enemy pos: " << result->pos.x << " " << result->pos.y << " " << result->pos.z << std::endl;

    }
        
}