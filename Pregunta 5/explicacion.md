# Pregunta 5

## Explicación:

### En mi solución, tome en cuenta la prioridad de las condiciones propuestas. La más importante era que el enemigo se encuentre renderizado; la segunda, que el enemigo se encuentre dentro de un radio de 20 unidades; por último, priorizar a los enemigos que se encontraban en un cono de visión. Aproveché la segunda condición para desarrollar la tercera, ya que gracias a ella ya no tenía que preocuparme por la distancia del enemigo al jugador, solo tenía que evaluar que el ángulo entre el vector enemigo y el vector de dirección del player sea menor que el ángulo del cono.

<br>

### Por otra parte, etiqueté a los enemigos dentro del cono de visión con el número 2 y a los que estaban fuera del cono, pero renderizados y dentro de la distancia de 20 unidades con el número 1. Los demás enemigos fueron etiquetados con 0 y no eran considerados por el sistema Lock-on.

<br>

### Para obtener el ángulo entre 2 vectores utilicé el producto escalar y otros métodos matemáticos que ya conocía.