#include <bits/stdc++.h>

struct Rectangle{
    float cx,cy,w,h;
    Rectangle(float x, float y, float w, float h) : cx(x), cy(y), w(w), h(h){}
};

bool collision(Rectangle a, Rectangle b){
    return (a.cx + a.w/2) > (b.cx - b.w/2) &&
            (a.cy + a.h/2) > (b.cy - b.h/2) &&
            (b.cx + b.w/2) > (a.cx - a.w/2) &&
            (b.cy + b.h/2) > (a.cy - a.h/2);
}

void getCollisions(Rectangle a, Rectangle b, Rectangle c){

    bool collisions[] = {collision(a,b),collision(b,c),collision(c,a)};
    
    if( collisions[0] && collisions[1] && collisions[2]){
        printf("Colisionan todos contra todos\n");
    }else {
        if (collisions[0]){
            printf("A y B colisionan\n");
        }
        if (collisions[1]){
            printf("B y C colisionan\n");
        }
        if (collisions[2]){
            printf("C y A colisionan\n");
        }
    }
}

int main(){

    float x,y,w,h;

    printf("Rectangulo A:\n");
    printf("xa: "); scanf("%d",&x);
    printf("ya: "); scanf("%d",&y);
    printf("wa: "); scanf("%d",&w);
    printf("ha: "); scanf("%d",&h);

    Rectangle rectA = Rectangle(x, y, w, h);

    printf("Rectangulo B:\n");
    printf("xb: "); scanf("%d",&x);
    printf("yb: "); scanf("%d",&y);
    printf("wb: "); scanf("%d",&w);
    printf("hb: "); scanf("%d",&h);

    Rectangle rectB = Rectangle(x, y, w, h);

    printf("Rectangulo C:\n");
    printf("xc: "); scanf("%d",&x);
    printf("yc: "); scanf("%d",&y);
    printf("wc: "); scanf("%d",&w);
    printf("hc: "); scanf("%d",&h);

    Rectangle rectC = Rectangle(x, y, w, h);
    
    // IMPRIME LAS COLISIONES
    getCollisions(rectA, rectB, rectC);

    return 0;
}