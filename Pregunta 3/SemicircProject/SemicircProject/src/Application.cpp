#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define PI 3.14159265

void drawPoints(int n, float r, float px, float py) {
    float segAngle = PI / (n-1);
    for (float i = 0; i < n; i++) {
        float x = px - cos(segAngle * i) * r;
        float y = py - sin(segAngle * i) * r;
        GLfloat pointVertex[] = { x,y };

        glEnableClientState(GL_VERTEX_ARRAY);
        glPointSize(2);
        glVertexPointer(2, GL_FLOAT, 0, pointVertex);
        glDrawArrays(GL_POINTS, 0, 1);
        glDisableClientState(GL_VERTEX_ARRAY);
    }
}

int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    int n;
    float r, px, py;
    std::cout << "Ingresar n: "; std::cin >> n;
    std::cout << "Ingresar r: "; std::cin >> r;
    std::cout << "Ingresar Px: "; std::cin >> px;
    std::cout << "Ingresar Py: "; std::cin >> py;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glViewport(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);

        drawPoints(n, r, px, py);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}