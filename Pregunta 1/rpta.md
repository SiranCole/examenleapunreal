# Pregunta 1
## ¿Qué hace el siguiente algoritmo?...

<br>

### El algoritmo en cuestión es una implementación del método división por tentativa, el cual sirve para verificar si un número "n" es primo o no. Retorna TRUE en caso sea primo y FALSE en caso no lo sea.